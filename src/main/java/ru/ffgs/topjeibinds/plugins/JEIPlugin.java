package ru.ffgs.topjeibinds.plugins;

import mezz.jei.api.IJeiRuntime;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.IRecipeRegistry;
import mezz.jei.api.IRecipesGui;
import mezz.jei.api.recipe.IFocus;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

@mezz.jei.api.JEIPlugin
public class JEIPlugin implements IModPlugin {
	private static IRecipesGui recipesGui;
	private static IRecipeRegistry recipeRegistry;

	@Override
	public void onRuntimeAvailable(@Nonnull IJeiRuntime jeiRuntime) {
		recipesGui = jeiRuntime.getRecipesGui();
		recipeRegistry = jeiRuntime.getRecipeRegistry();
	}

	public static void displayRecipe(@Nonnull ItemStack stack) {
		if (recipesGui == null || recipeRegistry == null || stack.isEmpty()) {
			return;
		}

		recipesGui.show(recipeRegistry.createFocus(IFocus.Mode.OUTPUT, stack));
	}

	public static void displayUsage(@Nonnull ItemStack stack) {
		if (recipesGui == null || recipeRegistry == null || stack.isEmpty()) {
			return;
		}

		recipesGui.show(recipeRegistry.createFocus(IFocus.Mode.INPUT, stack));
	}
}
