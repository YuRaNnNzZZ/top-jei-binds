package ru.ffgs.topjeibinds;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import ru.ffgs.topjeibinds.proxy.CommonProxy;

@Mod(
		modid = TOPJEIBinds.MOD_ID,
		dependencies = "required-after:theoneprobe; required-after:jei",
		clientSideOnly = true,
		acceptableRemoteVersions = "*",
		useMetadata = true
)
public class TOPJEIBinds {
	public static final String MOD_ID = "topjeibinds";

	@SidedProxy(clientSide = "ru.ffgs.topjeibinds.proxy.ClientProxy", serverSide = "ru.ffgs.topjeibinds.proxy.CommonProxy")
	public static CommonProxy proxy;

	@Mod.EventHandler
	public void onPreInit(FMLPreInitializationEvent event) {
		proxy.preInit(event);
	}

	@Mod.EventHandler
	public void onInit(FMLInitializationEvent event) {
		proxy.init(event);
	}
}
