package ru.ffgs.topjeibinds.event;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;
import ru.ffgs.topjeibinds.plugins.JEIPlugin;

@SideOnly(Side.CLIENT)
public class KeyBindHandler {
	private static final KeyBinding keyBindLookupRecipes = new KeyBinding("key.lookupRecipes", KeyConflictContext.IN_GAME, Keyboard.KEY_NUMPAD3, "key.categories.theoneprobe");
	private static final KeyBinding keyBindLookupUsage = new KeyBinding("key.lookupUsage", KeyConflictContext.IN_GAME, Keyboard.KEY_NUMPAD4, "key.categories.theoneprobe");

	public static void registerKeys() {
		ClientRegistry.registerKeyBinding(keyBindLookupRecipes);
		ClientRegistry.registerKeyBinding(keyBindLookupUsage);
	}

	@SubscribeEvent
	public void onKeyPress(InputEvent.KeyInputEvent event) {
		if (keyBindLookupRecipes.isPressed()) {
			JEIPlugin.displayRecipe(getLastTracedStack());
		}

		if (keyBindLookupUsage.isPressed()) {
			JEIPlugin.displayUsage(getLastTracedStack());
		}
	}

	private static ItemStack getLastTracedStack() {
		Minecraft mc = Minecraft.getMinecraft();
		World world = mc.world;
		RayTraceResult objectMouseOver = mc.objectMouseOver;

		BlockPos blockPos = objectMouseOver.getBlockPos();
		IBlockState blockState = world.getBlockState(blockPos);

		return blockState.getBlock().getPickBlock(blockState, objectMouseOver, world, blockPos, mc.player);
	}
}
