package ru.ffgs.topjeibinds.proxy;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import ru.ffgs.topjeibinds.event.KeyBindHandler;

public class ClientProxy extends CommonProxy {
    @Override
    public void preInit(FMLPreInitializationEvent e) {
        super.preInit(e);

        MinecraftForge.EVENT_BUS.register(new KeyBindHandler());
    }

    @Override
    public void init(FMLInitializationEvent e) {
        super.init(e);

        KeyBindHandler.registerKeys();
    }
}
